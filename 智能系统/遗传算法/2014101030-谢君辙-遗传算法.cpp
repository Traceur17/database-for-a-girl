#include<stdio.h>
#include<string.h>
#include<time.h>
#include<stdlib.h>
#include<math.h>
#include<algorithm>
#include<cmath>
using namespace std;

//LEN基因长度  NUM种群数量  GENMAX迭代代数  PC杂交概率  PM突变概率 
#define N 3000000
#define LEN 20 
#define NUM 100
#define GENMAX 2000 
#define PC 0.9
#define PM 0.1 
#define min(a,b) ((a)<(b)?(a):(b))

void init();		//初始化种群 
void cal_fitness(struct node *cur);	//各个种群适应度计算 
double random(int k);//0-k随机数 
void GA();				
int select();		//选择优者生成新种群 
void tran_best() ;	//换代函数（杂交和变异） 
int cmp(struct node a, struct node b) ;


typedef struct node
{
	char x1[LEN];		//个体信息
	char x2[LEN];
	double fitness;//适应度 
	double fitsum; //适应度总和 
};

double xx1,xx2; 	//函数两个变量 
node cur[NUM];	//总群变量 
node next[NUM];	
node MIN,MAX; 
node BEST[2*NUM+10];
int gennum;		//当前代数 


int main()
{
	int flag=1;
	char ch[10];
	while(flag)
	{
		memset(cur, 0, sizeof(cur));	//数组清空 
		memset(next, 0, sizeof(next));	//数组清空
		srand((unsigned)time(NULL));
		init();							//初始化种群 
		GA();							//遗传算法计算 
		printf("按1退出,其余键继续");
		scanf("%s",ch);
		if(ch[0]=='1')
			flag=0;
		printf("\n");
	}
	return 0;
}

void GA()
{
	double sum = 0;
	for (int i = 0; i < GENMAX; i++)
	{
		tran_best();
	}
	sum = cur[0].fitness;
	for (int i = 1; i < LEN; i++)
	{
		sum = min(sum, cur[i].fitness);
	}
	printf("x1 = %.6lf x2 = %.6lf\n", xx1, xx2);
	printf("最小值为：%.6lf\n", sum);
	printf("\n");
}

void cal_fitness(struct node *cur)
{
  int i,j,k1,k2;
  double d1,d2;
  for(i=0;i<NUM;i++)
  {
    k1=0;
    k2=0; 
    for(j=LEN-1;j>=0;j--) 
	{
		k1=(k1<<1)+cur[i].x1[j];
    }
	for(j=LEN-1;j>=0;j--) 
	{
		k2=(k2<<1)+cur[i].x2[j];
    }
    d1=(double)k1/N*6;
    d2=(double)k2/N*6;
    xx1=d1;
    xx2=d2;
    cur[i].fitness=3.0-sin(2*d1)*sin(2*d1)-sin(2*d2)*sin(2*d2);
    cur[i].fitsum=i>0?(cur[i].fitness+cur[i-1].fitsum):(cur[0].fitness);
  }
}

int cmp(struct node a, struct node b) 
{
	return a.fitness < b.fitness;
}

void tran_best() 
{
	//找到当前种群中最好的那个个体 
	MIN = cur[0];
	for (int i = 1; i<NUM; i++)
	{
		if (MIN.fitness>cur[i].fitness) 
		{
			MIN = cur[i];
		}
	}

	//杂交和变异 
	for (int i = 0; i < NUM; i += 2)
	{
		//选择交叉的个体 
		int p = select();
		int q = select();
		//定义交叉的位置 
		int pos = random(LEN - 1);
		
		//杂交 
		if (((double)rand() / RAND_MAX) < PC)
		{
			
			memcpy(next[i].x1, cur[p].x1, pos);
			memcpy(next[i].x1 + pos, cur[q].x1 + pos, LEN - pos);
			memcpy(next[i].x2, cur[p].x2, pos);
			memcpy(next[i].x2 + pos, cur[q].x2 + pos, LEN - pos);

			memcpy(next[i + 1].x1, cur[q].x1, pos);
			memcpy(next[i + 1].x1 + pos, cur[p].x1 + pos, LEN - pos);
			memcpy(next[i + 1].x2, cur[q].x2, pos);
			memcpy(next[i + 1].x2 + pos, cur[p].x2 + pos, LEN - pos);
		}
		else
		{
			memcpy(next[i].x1, cur[p].x1, LEN);
			memcpy(next[i].x2, cur[p].x2, LEN);
			memcpy(next[i + 1].x1, cur[q].x1, LEN);
			memcpy(next[i + 1].x2, cur[q].x2, LEN);
		}

		//变异
		if (((double)rand() / RAND_MAX) < PM)
		{
			int pos = random(LEN - 1); 
			next[i].x1[pos] ^= next[i].x1[pos];

			pos = random(LEN - 1); 
			next[i].x2[pos] ^= next[i].x2[pos];

			pos = random(LEN - 1); 
			next[i + 1].x1[pos] ^= next[i + 1].x1[pos];

			pos = random(LEN - 1); 
			next[i + 1].x2[pos] ^= next[i + 1].x2[pos];
		}
	}
	//找下一代的最差个体 
	cal_fitness(next);
	//完成后，从父代和子代中挑出最适合生存的
	memset(BEST, 0, sizeof(BEST));
	//用上一代的最优个体替换下一代的最差个体
	for (int i = 0; i < NUM; i++)
	{
		BEST[i] = next[i];
	}
	for (int j = 0; j < NUM; j++)
	{
		BEST[j + NUM] = cur[j];
	}
	sort(BEST, BEST + 2 * NUM, cmp);
	for (int i = 0; i < NUM; i++)
	{
		cur[i] = BEST[i];
	}
	cal_fitness(cur);
}

double random(int k)
{
	return (int)((double)rand() / RAND_MAX *k );
}

int select() 
{
	double p = (double)rand() / RAND_MAX ;
	double sum = cur[NUM-1].fitsum;
	for (int i=0; i<NUM; i++)
	{
		int f = cur[i].fitsum / sum; 
		if (f > p) 
			return i;
	}
	return ((int)rand())%NUM;
}

void init()
{
	int tmp1,tmp2;
	for(int i=0;i<NUM;i++)
	{
		tmp1=(random(N));
		tmp2=(random(N));
		for (int j = 0; j < LEN; j++)
		{
			cur[i].x1[j]=tmp1%2;
			tmp1/=2;
			cur[i].x2[j]=tmp2%2;
			tmp2/=2;
		}
	}
	cal_fitness(cur);
}
