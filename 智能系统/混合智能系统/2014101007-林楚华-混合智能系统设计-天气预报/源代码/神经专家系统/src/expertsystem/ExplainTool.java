package expertsystem;

/*解释工具*/
public class ExplainTool {
	public static void explainTool(int data[],float a)
	{
		System.out.print("今天");
		if(data[0] == 1)
		{
			System.out.print("有云，");
		}
		if(data[0] == 0)
		{
			System.out.print("没云，");
		}
		if(data[1] == 1)
		{
			System.out.print("有太阳，");
		}
		if(data[1] == 0)
		{
			System.out.print("没太阳，");
		}
		if(data[2] == 1)
		{
			System.out.print("有动物的异常行动，");
		}
		if(data[2] == 0)
		{
			System.out.print("没有动物的异常行动，");
		}
		if(data[3] < 20)
		{
			System.out.print("温度是"+data[3]+"，属于较低温度，");
		}
		if(data[3] < 25)
		{
			System.out.print("温度是"+data[3]+"，属于正常温度，");
		}
		if(data[3] >= 25)
		{
			System.out.print("温度是"+data[3]+"，属于较高温度，");
		}
		if(data[4] < 60)
		{
			System.out.print("湿度是"+data[4]+"%，空气较为干燥，");
		}
		if(data[4] < 75)
		{
			System.out.print("湿度是"+data[4]+"%，空气湿度正常，");
		}
		if(data[4] >= 75)
		{
			System.out.print("湿度是"+data[4]+"%，空气湿度较高，");
		}
		if(a<0.4)
		{
			System.out.print("是晴天");
			if(data[4]<75)
			{
				System.out.print("，适宜出行。");
			}
			else System.out.print("，但不适宜出行。");
		}
		if((a>0.4)&&(a<0.6))
		{
			System.out.print("是阴 天");
			if(data[4]<75)
			{
				System.out.print("，暂时不会下雨。");
			}
			else System.out.print("，出门记得带伞。");
		}
		if(a>0.6)
		{
			System.out.print("是雨天");
			if(data[4]<75)
			{
				System.out.print("，尽量不要出门啦~");
			}
			else System.out.print("，出门记得带伞~");
		}
	}
}
