package expertsystem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Random;

public class Knowledge {

	static Weather weather[] = new Weather[200];
	static point hidden[] = new point[12];//设置隐含层的点
	static point last[] = new point[3];//设置输出层的点
	
	static Float w1[][] = new Float[5][12];//设置输入层和隐含层的权重
	static Float w2[][] = new Float[12][3];//设置隐含层和输出层的权重
	
	//初始化天气数据
	public static void InitWeather()
	{
		String dataString[][] = new String[200][6];
		//读取神经知识库.txt数据文件
				try {
				       String encoding="GBK";
				       File file=new File("F:\\Eclipse\\workspace\\神经专家系统\\src\\expertsystem\\神经知识库.txt");
				       if(file.isFile() && file.exists()){ //判断文件是否存在
				           InputStreamReader read = new InputStreamReader(
				           new FileInputStream(file),encoding);//考虑到编码格式
				           BufferedReader bufferedReader = new BufferedReader(read);
		                   String lineTxt = null;
		                   int i=0;
		                   while((lineTxt = bufferedReader.readLine()) != null){
		                	   if(!lineTxt.trim().isEmpty())
		                	   {
		                		   dataString[i]=lineTxt.split(",");
		                		   weather[i] = new Weather();
		                		   weather[i].weather(Integer.parseInt(dataString[i][0]),
		                				   			Integer.parseInt(dataString[i][1]),
		                				   			Integer.parseInt(dataString[i][2]),
		                				   			Integer.parseInt(dataString[i][3]),
		                				   			Integer.parseInt(dataString[i][4]));
		                		   weather[i].Weather = Integer.parseInt(dataString[i][5]);
		                		   i++;
		                	   }
		                   }
		                   read.close();
				       }else{
				       		System.out.println("找不到指定的文件");
				       	 }
					} catch (Exception e) {
						System.out.println("读取文件内容出错");
						e.printStackTrace();
					}
	}
	//设置权值在-0.5,0.5之间
	public static void InitWeight()
	{
		Random ran1 = new Random();
		Random ran2 = new Random();
		Random ran3 = new Random();
		int n = 1;
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<12;j++)
			{
				if(ran3.nextBoolean())
				{
					n = -1;
				}
				w1[i][j] = n*(float)ran1.nextInt(5)/10;
			}
		}
		
		for(int i=0;i<12;i++)
		{
			for(int j=0;j<3;j++)
			{
				if(ran3.nextBoolean())
				{
					n = -1;
				}
				w2[i][j] = n*(float)ran2.nextInt(5)/10;
			}
		}
	}
	
	public static void InitPoint()
	{
		Random ran = new Random();
		Random ran3 = new Random();
		int n=1;
		for(int i=0;i<12;i++)
		{
			hidden[i] = new point(); 
			if(ran3.nextBoolean())
			{
				n = -1;
			}
			hidden[i].theta = n*(float)ran.nextInt(5)/10;
		}
		for(int i=0;i<3;i++)
		{
			last[i] = new point();
			if(ran3.nextBoolean())
			{
				n = -1;
			}
			last[i].theta = n*(float)ran.nextInt(5)/10;
		}
		
		last[0].hope =0.1f;
		last[1].hope =0.5f;
		last[2].hope =0.9f;
	}
	public static void knowledge()
	{
		InitWeather();
		InitWeight();
		InitPoint();
		
		
		float alpha = 0.1f;//学习速率
		
		for(int a=0;a<400;a++)
		{
			for(int b=0;b<20;b++)
			{
				for(int j=0;j<12;j++)
				{
					hidden[j].output = (float)(1/(1+Math.exp(-(weather[b].Cloud*w1[0][j]+
													weather[b].Sun*w1[1][j]+
													weather[b].Animal*w1[2][j]+
													weather[b].Temperature*w1[3][j]+
													weather[b].dampness*w1[4][j]-hidden[j].theta))));
				}
				
				for(int g=0;g<3;g++)
				{
					last[g].err = 0.0f;
					last[g].delta = 0.0f;
				}
				
				int k = 0;
				if(weather[b].Weather == 1) k = 0;
				if(weather[b].Weather == 0) k = 1;
				if(weather[b].Weather == -1) k = 2;
				last[k].output = (float)(1/(1+Math.exp(-(hidden[0].output*w2[0][k]+
															hidden[1].output*w2[1][k]+
															hidden[2].output*w2[2][k]+
															hidden[3].output*w2[3][k]+
															hidden[4].output*w2[4][k]+
															hidden[5].output*w2[5][k]+
															hidden[6].output*w2[6][k]+
															hidden[7].output*w2[7][k]+
															hidden[8].output*w2[8][k]+
															hidden[9].output*w2[9][k]+
															hidden[10].output*w2[10][k]+
															hidden[11].output*w2[11][k]-last[k].theta))));
				last[k].err = last[k].hope-last[k].output;//计算误差
				last[k].delta = last[k].output*(1-last[k].output)*last[k].err;//计算误差梯度
				
				
				//更新隐含层与输出层之间的权重
				for(int i=0;i<12;i++)
				{
					for(int j=0;j<3;j++)
					{
						w2[i][j] += alpha*hidden[i].output*last[j].delta;
					}
				}
				
				//计算隐含层神经元的误差梯度
				for(int i=0;i<12;i++)
				{
					for(int j=0;j<3;j++)
					{
						hidden[i].err += (last[j].delta*w2[i][j]);
					}
					hidden[i].delta = hidden[i].output*(1-hidden[i].output)*hidden[i].err;
				}
				
				//更新输入层和隐含层的权重
				for(int i=0;i<5;i++)
				{
					for(int j=0;j<12;j++)
					{
						if(i==0)
						{
							w1[i][j] += alpha*weather[b].Cloud*hidden[j].delta;
						}
						if(i==1)
						{
							w1[i][j] += alpha*weather[b].Sun*hidden[j].delta;
						}
						if(i==2)
						{
							w1[i][j] += alpha*weather[b].Animal*hidden[j].delta;
						}
						if(i==3)
						{
							w1[i][j] += alpha*weather[b].Temperature*hidden[j].delta;
						}
						if(i==4)
						{
							w1[i][j] += alpha*weather[b].dampness*hidden[j].delta;
						}
					}
				}
				
//				if((Math.pow(last[0].err,2) < 0.001)
//						&&(Math.pow(last[1].err,2) < 0.001)
//						&&(Math.pow(last[2].err,2) <0.001))
//				{
//					break;
//				}	
			}
			if((Math.pow(last[0].err,2) < 0.00001)
					&&(Math.pow(last[1].err,2) < 0.00001)
					&&(Math.pow(last[2].err,2) <0.0000000000001))
			{
				System.out.println("已经训练完成！");
				break;
			}
		}
		//计算准确率
		int all=0,right = 0;
		
		for(int i=0;i<20;i++)
		{
			for(int j=0;j<12;j++)
			{
				hidden[j].output = (float)(1/(1+Math.exp(-(weather[i].Cloud*w1[0][j]+
												weather[i].Sun*w1[1][j]+
												weather[i].Animal*w1[2][j]+
												weather[i].Temperature*w1[3][j]+
												weather[i].dampness*w1[4][j]-hidden[j].theta))));
			}
			for(int k=0;k<3;k++)
			{
				last[k].output = (float)(1/(1+Math.exp(-(hidden[0].output*w2[0][k]+
														hidden[1].output*w2[1][k]+
														hidden[2].output*w2[2][k]+
														hidden[3].output*w2[3][k]+
														hidden[4].output*w2[4][k]+
														hidden[5].output*w2[5][k]+
														hidden[6].output*w2[6][k]+
														hidden[7].output*w2[7][k]+
														hidden[8].output*w2[8][k]+
														hidden[9].output*w2[9][k]+
														hidden[10].output*w2[10][k]+
														hidden[11].output*w2[11][k]-last[k].theta))));
			}
			if(last[0].output <last[1].output)
			{
				if((last[0].output <last[2].output)&&(weather[i].Weather == 1))
				{
					all++;
					right++;
				}
				else{
					if((weather[i].Weather == -1)&&(last[0].output >last[2].output))
					{
						all++;
						right++;
					}
				}
			}
			else if((last[1].output <last[2].output)&&(weather[i].Weather == 0))
			{
				all++;
				right++;
			}
			else{
				if((weather[i].Weather == -1)&&(last[1].output > last[2].output))
				{
					all++;
					right++;
				}
				else all++;
			}
		}
		
		System.out.println("准确率为"+(float)right/all*100+"%");
		
	}
	
	public static float test(int data[])
	{
		for(int j=0;j<12;j++)
		{
			hidden[j].output = (float)(1/(1+Math.exp(-(data[0]*w1[0][j]+
											data[1]*w1[1][j]+
											data[2]*w1[2][j]+
											data[3]*w1[3][j]+
											data[4]*w1[4][j]-hidden[j].theta))));
		}
		Random r = new Random();
		int i = r.nextInt(2);
		for(int k=0;k<3;k++)
		{
			last[k].output = (float)(1/(1+Math.exp(-(hidden[0].output*w2[0][k]+
													hidden[1].output*w2[1][k]+
													hidden[2].output*w2[2][k]+
													hidden[3].output*w2[3][k]+
													hidden[4].output*w2[4][k]+
													hidden[5].output*w2[5][k]+
													hidden[6].output*w2[6][k]+
													hidden[7].output*w2[7][k]+
													hidden[8].output*w2[8][k]+
													hidden[9].output*w2[9][k]+
													hidden[10].output*w2[10][k]+
													hidden[11].output*w2[11][k]-last[k].theta))));
		}
		
		return last[i].output;
	}
	
}









