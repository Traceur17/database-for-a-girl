CREATE VIEW VS3
AS SELECT SNO,SNAME,STATUS,CITY
FROM s
WHERE CITY='北京'
WITH CHECK OPTION;

insert
into VS3
VALUES('S7','超级丰盛','17','北京');

-- delete from vs3 where sno='s7';

insert
into VS3
VALUES('S8','超级超级丰盛','17','天津');

CREATE VIEW VS4
AS SELECT SNO,SNAME,STATUS,CITY
FROM s
WHERE CITY='北京';

insert
into VS4
VALUES('S7','超级丰盛','17','北京');

insert
into VS4
VALUES('S8','超级超级丰盛','17','天津');
