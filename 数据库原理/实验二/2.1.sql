USE spl;
CREATE VIEW VS1
AS SELECT SNO,SNAME,STATUS
FROM s
WHERE CITY='北京'
WITH CHECK OPTION;

INSERT
INTO VS1
VALUES('S6','丰盛',20);

USE spl;
CREATE VIEW VS2
AS SELECT SNO,SNAME,STATUS
FROM s
WHERE CITY='北京';

INSERT
INTO VS2
VALUES('S6','丰盛',20);