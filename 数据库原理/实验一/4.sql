-- 4、求没有使用天津供应商生产的红色零件的工程号JNO

SELECT
	JNO
FROM
	j
WHERE NOT EXISTS (
	SELECT                          #完善所有工程号
		S.*,spj.*,p.* 
	FROM S,SPJ,P 
	WHERE SPJ.JNO=J.JNO 
		AND SPJ.SNO=S.SNO
        AND SPJ.PNO=P.PNO
		AND S.CITY='天津'
		AND p.COLOR='红');