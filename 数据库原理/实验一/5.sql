-- 5、求至少用了供应商S1所供应的全部零件的工程号JNO

USE SPL;
SELECT DISTINCT 
	JNO 
FROM 
	SPJ SPJX 
WHERE NOT EXISTS (
	SELECT 
		* 
	FROM 
		SPJ SPJY
	WHERE 
		SPJY.SNO='S1'
	AND NOT EXISTS(
		SELECT
			*
		FROM 
			SPJ SPJZ
		WHERE SPJZ.JNO=SPJX.JNO 
			AND SPJZ.PNO=SPJY.PNO
			AND SPJZ.SNO=SPJY.SNO));