
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>   
#include <time.h> 
#include <math.h>

static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))
#define HANDLE_NULL( a ) {if (a == NULL) { \
                            printf( "Host memory failed in %s at line %d\n", \
                                    __FILE__, __LINE__ ); \
                            exit( EXIT_FAILURE );}}

const int N = 1024*1024*64;     	//积分时划分的份数
const int threadsPerBlock = 256;	//block中的线程数
const int blocksPerGrid = 64;		//grid中的block数

//CPU运行，CPU调用
double function_for_cpu(double x) {
    return 4/(1+x*x);
}

//GPU运行，GPU调用
__device__ double function_for_gpu(double x) {
	return 4/(1+x*x);
}

//GPU运行，CPU调用
__global__ void trap(double *a, double *b, double *integral ) {
    __shared__ double cache[threadsPerBlock];
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int cacheIndex = threadIdx.x;

    double   x, temp = 0;
    while (tid < N) {
		x = *a + (double)(*b-*a)/N*(tid+0.5) ;
        temp += function_for_gpu(x);
        tid += blockDim.x * gridDim.x;
    }
    
    // set the cache values
    cache[cacheIndex] = temp;
    
    // synchronize threads in this block
    __syncthreads();

    // for reductions, threadsPerBlock must be a power of 2
    // because of the following code
    int i = blockDim.x/2;
    while (i != 0) {
        if (cacheIndex < i)
            cache[cacheIndex] += cache[cacheIndex + i];
        __syncthreads();
        i /= 2;
    }

    if (cacheIndex == 0)
        integral[blockIdx.x] = cache[0];
}

void trap_by_cpu(double a, double b, double *integral) {
	int i;
	double x, temp = 0;
	for (i=0; i<N; i++) {
		x = a + (double)(b-a)/N*(i+0.5);
        temp += function_for_cpu(x);
	}
	temp *= (double)(b-a)/N;
	*integral = temp;
}

int main( void ) {
    double   integral;
	double	*partial_integral;
	double a, b;

    double   *dev_partial_integral;
	double *dev_a, *dev_b;
	cudaEvent_t start,stop;
	float tm;  
    
	clock_t  clockBegin, clockEnd;    
    float duration; 

	a = 0;
	b = 1;
    
  	//by CPU
	clockBegin = clock();  
	trap_by_cpu(a, b, &integral);
	clockEnd = clock();    
	duration = (float)1000*(clockEnd - clockBegin) / CLOCKS_PER_SEC;   
	printf("CPU Result: %.20lf\n", integral);
	printf("CPU Elapsed time: %.6lfms\n", duration );  
	
	getchar();

	//by GPU+CPU
	cudaEventCreate(&start);  
    cudaEventCreate(&stop); 
	cudaEventRecord(start,0); 

	// allocate the memory on the Memory for CPU
    partial_integral = (double*)malloc( blocksPerGrid*sizeof(double) );

    // allocate the memory on the GPU
	HANDLE_ERROR( cudaMalloc( (void**)&dev_a, sizeof(double) ) );
	HANDLE_ERROR( cudaMalloc( (void**)&dev_b, sizeof(double) ) );
    HANDLE_ERROR( cudaMalloc( (void**)&dev_partial_integral,
                              blocksPerGrid*sizeof(double) ) );

	HANDLE_ERROR( cudaMemcpy( dev_a, &a, sizeof(double),cudaMemcpyHostToDevice ) );
	HANDLE_ERROR( cudaMemcpy( dev_b, &b, sizeof(double),cudaMemcpyHostToDevice ) );
    
    trap<<<blocksPerGrid,threadsPerBlock>>>( dev_a, dev_b, dev_partial_integral );

    // copy the array back from the GPU to the CPU
    HANDLE_ERROR( cudaMemcpy( partial_integral, dev_partial_integral,
                              blocksPerGrid*sizeof(double),
                              cudaMemcpyDeviceToHost ) );

    // finish up on the CPU side
    integral = 0;
    for (int i=0; i<blocksPerGrid; i++) {
        integral += partial_integral[i];
    }
	integral *= (double)(b-a)/N;

	cudaEventRecord(stop,0);  
    cudaEventSynchronize(stop);  
    cudaEventElapsedTime(&tm, start, stop);  
	printf("GPU Result: %.20lf\n", integral);
    printf("GPU Elapsed time:%.6f ms.\n",tm); 

    // free memory on the gpu side
	HANDLE_ERROR( cudaFree( dev_a ) );
	HANDLE_ERROR( cudaFree( dev_b ) );
    HANDLE_ERROR( cudaFree( dev_partial_integral ) );

    // free memory on the cpu side
    free( partial_integral );

	getchar();
}
