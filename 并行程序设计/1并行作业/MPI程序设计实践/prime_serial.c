/*
用MPI并行化
0号进程当manager，其他进程当worker
输入一个命令行参数chunk_size（argv中的，而不是scanf输入的）
worker每次请求，得到chunk_size个数（最后一次可能得到的少于chunk_size个）
无需测试运行时间的长短，只需测试功能上的正确性
可以在自己的MPI环境中进行测试；也可以ssh到192.168.69.2，用户名pp，密码pp123456，进行测试(每人可建一个目录)，运行时无需指定-f的参数
测试3个进程时和4个进程时的结果
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct timeval begin_time, end_time;
double run_time_ms;

int main(int argc, char *argv[])
{
	long i, j, limit;
	long end;          /* range of numbers to search */
	long primecount=0; /* number of primes found */
	int prime;               /* is the number prime? */

	gettimeofday(&begin_time, NULL);

	end = 80000000;

	printf("Range to count Primes: 1 - %d\n", end);

    for(i = 3; i <= end; i += 2) 
	{
		limit = (long) sqrt((double)i);
		prime = 1; /* assume number is prime */
		j = 3;

		while (j <= limit) {
			if (i%j == 0) {
				prime = 0;
				break;
			}

			j += 2;
		}

		if (prime) {
		    primecount++;
		}
	}

    primecount++;   //2 is a prime

	printf("\n%d primes found\n", primecount);

    gettimeofday(&end_time, NULL);
    run_time_ms =
        (end_time.tv_sec - begin_time.tv_sec)*1000 +
        (end_time.tv_usec - begin_time.tv_usec)*1.0/1000;
    printf("run_time = %lfms\n", run_time_ms);

	return 0;
}