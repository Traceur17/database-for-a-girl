#include <stdio.h>
#include <stdlib.h>

//////////////////////////////////////////////////////////////////////
//请在此添加必要的头文件
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//请在此定义数据结构和声明函数
struct Param{
	int id;
	float min_meas;
	int count;
	float *data;
	float *bin_maxes;
	long bin_count;
	long *bin_counts;
};

void *Func(void *arg  /*in*/) ;
//////////////////////////////////////////////////////////////////////

void Usage(char prog_name[]);

void Get_args(
	char*    argv[]        /* in  */,
	long*     bin_count_p   /* out */,
	float*   min_meas_p    /* out */,
	float*   max_meas_p    /* out */,
	long*     data_count_p  /* out */,
	int*  thread_count_p  /* out */);

void Gen_data(
	float   min_meas    /* in  */,
	float   max_meas    /* in  */,
	float   data[]      /* out */,
	long     data_count  /* in  */);

void Gen_bins(
	float min_meas      /* in  */,
	float max_meas      /* in  */,
	float bin_maxes[]   /* out */,
	long   bin_counts[]  /* out */,
	long   bin_count     /* in  */);

long Which_bin(
	float    data         /* in */,
	float    bin_maxes[]  /* in */,
	long      bin_count    /* in */,
	float    min_meas     /* in */);

void Print_histo(
	float    bin_maxes[]   /* in */,
	long      bin_counts[]  /* in */,
	long      bin_count     /* in */,
	float    min_meas      /* in */);
	

//////////////////////////////////////////////////////////////////////

//请修改该函数
int main(int argc, char* argv[])
{
	long bin_count, i, bin;
	float min_meas, max_meas;
	float* bin_maxes;
	long* bin_counts;
	long data_count;
	int thread_count;
	float* data;

	struct timeval begin_time, end_time;
	double run_time_ms;

	/* Check and get command line args */
	if (argc != 6) Usage(argv[0]);
	Get_args(argv, &bin_count, &min_meas, &max_meas, &data_count, &thread_count);

	/* Allocate arrays needed */
	bin_maxes = malloc(bin_count * sizeof(float));
	bin_counts = malloc(bin_count * sizeof(long));
	data = malloc(data_count * sizeof(float));

	/* Generate the data */
	Gen_data(min_meas, max_meas, data, data_count);

	/* Create bins for storing counts */
	Gen_bins(min_meas, max_meas, bin_maxes, bin_counts, bin_count);

	gettimeofday(&begin_time, NULL);

	/*
	请将下面这个for循环用pthread多线程并行化
	要求及注意事项：
	1）不允许使用全局变量；
	2）程序的命令行（即main函数）在最后面增加一个参数thread_count，表示线程数量；
	3）malloc的内存要free；
	4）除main, Get_args和Usage函数外，请不要修改其他函数，当然可能需要增加函数和一些类型的定义；
	5）先用./histogram 10 0 1 200000000测试单线程的运行时间，然后分别以10 0 1 200000000 2和10 0 1 200000000 4作为参数进行测试；
	6）Which_bin函数参数的意义见函数实现处的说明。
	*/
	/* Count number of values in each bin */
	
	struct Param param[thread_count];
	pthread_t pth[thread_count];
	int rtn;
	
	for(i=0;i<thread_count;i++)
	{
		param[i].bin_count = bin_count;
		param[i].bin_maxes = bin_maxes;
		param[i].bin_counts = bin_counts;
		param[i].data = data;
		param[i].count = (int)data_count/thread_count;
		param[i].min_meas = min_meas;
		param[i].id = i;
		
		rtn = pthread_create(&pth[i] , NULL , Func , (void*)(&param[i]));
		
		if(rtn)
		{
			printf("ERROR; return code from pthread_create() is %d\n", rtn);
         	exit(1);
		}
	}

	for(i = 0 ; i<thread_count ; i++)
	{
		pthread_join(pth[i],NULL);
	}
	
	gettimeofday(&end_time, NULL);
    run_time_ms =
        (end_time.tv_sec - begin_time.tv_sec)*1000 +
        (end_time.tv_usec - begin_time.tv_usec)*1.0/1000;
    printf("run_time = %lfms\n", run_time_ms);

	/* Print the histogram */
	Print_histo(bin_maxes, bin_counts, bin_count, min_meas);

	free(data);
	free(bin_maxes);
	free(bin_counts);
	return 0;
}  /* main */

   //请修改该函数
void Usage(char prog_name[] /* in */)
{
	fprintf(stderr, "usage: %s ", prog_name);
	fprintf(stderr, "<bin_count> <min_meas> <max_meas> <data_count> <thread_count>\n");
	exit(0);
}  /* Usage */

   //请修改该函数
void Get_args(
	char*    argv[]        /* in  */,
	long*     bin_count_p   /* out */,
	float*   min_meas_p    /* out */,
	float*   max_meas_p    /* out */,
	long*     data_count_p  /* out */,
	int*  thread_count_p  /* out */)
{
	*bin_count_p = strtol(argv[1], NULL, 10);
	*min_meas_p = strtof(argv[2], NULL);
	*max_meas_p = strtof(argv[3], NULL);
	*data_count_p = strtol(argv[4], NULL, 10);
	*thread_count_p = atoi(argv[5]);
}  /* Get_args */


void Gen_data(
	float   min_meas    /* in  */,
	float   max_meas    /* in  */,
	float   data[]      /* out */,
	long     data_count  /* in  */)
{
	long i;
	srand(0);
	for (i = 0; i < data_count; i++)
		data[i] = (float)(min_meas + (max_meas - min_meas)*rand() / ((double)RAND_MAX));
}  /* Gen_data */


void Gen_bins(
	float min_meas      /* in  */,
	float max_meas      /* in  */,
	float bin_maxes[]   /* out */,
	long   bin_counts[]  /* out */,
	long   bin_count     /* in  */)
{
	float bin_width;
	long   i;

	bin_width = (max_meas - min_meas) / bin_count;

	for (i = 0; i < bin_count; i++) {
		bin_maxes[i] = min_meas + (i + 1)*bin_width;
		bin_counts[i] = 0;
	}
}  /* Gen_bins */

   /*---------------------------------------------------------------------
   * Function:  Which_bin
   * Purpose:   Use binary search to determine which bin a measurement
   *            belongs to
   * In args:   data:       the current measurement,				数据
   *            bin_maxes:  list of max bin values,				每个bin的上边界
   *            bin_count:  number of bins,						bin的数量
   *            min_meas:   the minimum possible measurement,		数据的最小值
   * Return:    the number of the bin to which data belongs, 		bin的编号
   * Notes:
   * 1.  The bin to which data belongs satisfies
   *
   *            bin_maxes[i-1] <= data < bin_maxes[i]
   *
   *     where, bin_maxes[-1] = min_meas
   * 2.  If the search fails, the function prlongs a message and exits
   */
long Which_bin(
	float   data          /* in */,
	float   bin_maxes[]   /* in */,
	long     bin_count     /* in */,
	float   min_meas      /* in */)
{
	long bottom = 0, top = bin_count - 1;
	long mid;
	float bin_max, bin_min;

	while (bottom <= top) {
		mid = (bottom + top) / 2;
		bin_max = bin_maxes[mid];
		bin_min = (mid == 0) ? min_meas : bin_maxes[mid - 1];
		if (data >= bin_max)
			bottom = mid + 1;
		else if (data < bin_min)
			top = mid - 1;
		else
			return mid;
	}
	
	return top;    //bug? added by XiongZhi

				   /* Whoops! */
	printf("bottom = %ld, top = %ld\n", bottom, top);
	fprintf(stderr, "Data = %f doesn't belong to a bin!\n", data);
	fprintf(stderr, "Quitting\n");
	exit(-1);
}  /* Which_bin */


void Print_histo(
	float  bin_maxes[]   /* in */,
	long    bin_counts[]  /* in */,
	long    bin_count     /* in */,
	float  min_meas      /* in */)
{
	long i;
	float bin_max, bin_min;

	for (i = 0; i < bin_count; i++) {
		bin_max = bin_maxes[i];
		bin_min = (i == 0) ? min_meas : bin_maxes[i - 1];
		printf("%.3f-%.3f:\t", bin_min, bin_max);
		//for (j = 0; j < bin_counts[i]; j++)
		// printf("X");
		printf("%ld", bin_counts[i]);  //altered by XiongZhi
		printf("\n");
	}
}  /* Print_histo */

   //////////////////////////////////////////////////////////////////////
   //请在给出所增加函数的实现
void *Func(void *arg)
{
	struct Param param = *((struct Param *)arg);
	int id = param.id;
	
	int j = param.count;
	int i,bin;
	
	for(i = j*id; i<j*(id+1);i++)
	{
		bin = Which_bin(param.data[i], param.bin_maxes, param.bin_count, param.min_meas);
		param.bin_counts[ bin ]++;
	}
	
	pthread_exit(NULL);
}
   //////////////////////////////////////////////////////////////////////

