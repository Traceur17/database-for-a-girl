/**
该程序用于统计完全数（自行搜索其定义）的数量。
命令行带一个参数n，表示统计从1~n之间的完全数。
该程序编译链接时要带选项-fopenmp -lm。
在上课用的服务器上，n=2000000时，该程序的运行时间约为37.4秒。
请用OpenMP并行化该程序。要求如下：
1、命令行多带一个参数m，表示用m个线程并行化。
2、以参数n=2000000，m=2和4进行测试，给出测试结果。
3、注意n是由用户输入的，要考虑n可能较小时的情形。
4、本题目的目的是训练和测试OpenMP，而不是算法，因此请不要修改算法。
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

int main(int argc, char *argv[])
{
	long i, j, limit;
	long n;
	long count;
	long sum;
	int thread_num;
	double start, end;

	if (argc != 3) {
		printf("Usuage: %s <range> <thread_num> \n", argv[0]);
		return -1;
	}

	n = atol(argv[1]);
	thread_num = atoi(argv[2]);
	start = omp_get_wtime();

	count = 1;   //1 is perfect, 2 and 3 are not.


#pragma omp parallel for num_threads(thread_num) private(sum,i,j,limit) shared(count) schedule(dynamic, 1000)
	for (i = 4; i <= n; i++) {

		limit = (long)sqrt((double)i);
		sum = 1;
		for (j = 2; j <= limit; j++) {
			if (i%j == 0) {
				sum += i / j + j;
			}
		}

		if (sum == i) {
			printf("%d\n", i);
			count++;
		}
	}

	printf("\n%d perfect number are found in [%d, %d]\n", count, 1, n);

	end = omp_get_wtime();
	printf("run_time = %f seconds\n", end - start);

	return 0;
}