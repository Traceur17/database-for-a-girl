题目：客户端向服务器对象请求调用一个方法sum()，对传入的一个整型数组求和，服务器返回该数组的和（可以假定数组的大小最大为32个元素），
客户程序打印该返回值。可根据需要作出某些合理的假设（比如原文文件所在的目录，编译器的厂商等），但是必须明确说明。
答案：
假设WSDL文件地址是http://www.stu.edu.cn/services/test?wsdl
客户端代码如下
package exam;
import ...;  /*...省略部分内容*/
...
public class Client{   
   public static void main(String[]rags){            
         try{            
               Stringendpoint="http://stu.edu.cn/services/test?wsdl"
               Service service = new Service();// 创建一个服务(service)调用(call)
               Call call = (call) service.createCall();// 通过service创建call对象
               callsetTargetEndpointAddress(endpoint);//设置service所在URL,endpoint为wsdl地址
               call.setOperationName("sum");//operationName为操作名
               call.addParameter("parameter", org.apache.axis.Constants.XSD_STRING,javax.xml.rpc.ParameterMode.IN);
               call.setReturnType(org.apache.axis.Constants.XSD_STRING);
               int test[]={1,2,3,4,5};
               int result=(int)call.invoke(newObject[]{test[]});
               System.out.println("the result is" + result);
            } 
         catch(Exception e){
            System.err.println(e.toString());                     
            }
      } 
}
备注：客户端代码基本上是通用代码，题目不同只是改一下test数组部分内容

服务器代码如下
package exam;
public interface ssum{
   public int sum(int input[32]);
}
package Iexam;
import exam.ssum;
public classImpsum implements ssum{
   public int sum(int input[32]){
      int sum=0;
      for (int i=0; i<input[].size; i++){
         sum = sum+input[i];
      }
   return sum;
   }
}

services.xml
<?xml versiom ="1.0" encoding"UTF-8"?>
<beans xmlns="http://xfine.codehaus.org/config./1.0">
<service>
<name>Test</name>
<serviceclass>exam.ssum</serviceclass>
<implementationclass>Iexam.Impsum</implementationclass>
</service>
</beans>