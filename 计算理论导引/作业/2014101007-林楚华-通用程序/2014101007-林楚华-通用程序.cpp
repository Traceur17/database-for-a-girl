#include<string.h>
#include<iostream>
using namespace std;
#define max_n 100 

//判断x是不是素数 
int Prim(int x) 
{
	for(int i=2;i*i<=x;i++)
	{
		if(x%i==0)
		{
			return 0;
		}
	}
	return 1;
}

//素因子分解中非0指数最大素数的序号 ，即求得哥德尔数组的长度 
int Lt(int x)
{
      int max_value=x;
      int prim_index=0;
	  for(int i=2;i<max_value&&x!=1;i++)
	  {
	  	//	int flag=0;
	  		if(Prim(i)==1)
	  		{
	  		//	cout<<x<<" "<<i<<endl; 
	  			prim_index++;
	  			//i是素数
				while(x%i==0)
				{
					x=x/i;
				}
	  		}
	
	 }
	 cout<<"最大素数的序号"<<prim_index<<endl;
	 return prim_index; 
}

//判断x是不是哥德尔数 
int PROG(int x)
{
	int i=2;
	int sum=1;
	while(i>0&&x>1)
	{
		if(Prim(i)==1)
		{
			//是素数
			if(x%i!=0)
			{
				return 0;
			}
			while(x%i==0)
			{
				x/=i;
			}
		}
		i++;
	}
	if(x==1)
	{
		//是哥德尔数
		return 1; 
	}
	else
	{
		return 0;
	}
} 
//Cnator配对 
//把一个数唯一还原为两个数 ，取右部 
int r(int z)
{
	for(int i=0;i<z;i++)
	{
		for(int j=0;j<z;j++)
		{
			if((i+j)*(i+j+1)/2+j==z)
			{
				cout<<z<<"的右部为"<<j<<endl; 
				return j;
			}
		}		
	}
} 
//Cnator配对 ，取左部 
int l(int z)
{
	for(int i=0;i<z;i++)
	{
		for(int j=0;j<z;j++)
		{
			if((i+j)*(i+j+1)/2+j==z)
			{ 
				cout<<z<<"的左部为"<<i<<endl; 
				return i;
			}
		}		
	}

} 

//求第i个素数的指数 
int X_i(int x,int index)
{
	  if(index==0) return 0;
	  int count=0; //第i个素数 
      int max_value=x;
	  for(int i=2;i<=max_value/2;i++)
	  {
	  		int flag=0;
	  		if(Prim(i)==1)
	  		{
	  				//i是素数
	  			count++;
				while(x%i==0)
				{
					flag=1;
					x=x/i;
				}
				if(count==index)
					{
						int num=0;
						while(max_value%i==0)
						{
							num++;
							max_value/=i;
						}
						cout<<"第"<<index<<"个指数："<<num<<"当前x="<<x<<endl;
						return num;
					}
				
	  		}
	 }	  
}

 
int main()
{
	char belt[max_n];//带子 
	char *p; 
	int n=0,i=0,w;
	int j;
	//判断输入的数是不是哥德尔数
	cin>>n; 
	if(!PROG(n))
	{
		cout<<"error"<<endl;
		return 0;
	}

	i=1;//初始化指令计数器
	p=belt;//初始化纸带指针在最前面 
	//先初始化纸带 
	for(int index=0;index<100;index++)
	{
		belt[index]='B';
	} 

	A://如果i=0则没有指令，i>Lt(n)则是指令执行完毕 
	if(i==0||i>Lt(n))
	{
	   cout<<"i="<<i<<endl;
	   cout<<"结束"<<endl;	
	   goto G;	
	} 
	//右部为1，执行右移指令 
	if(r(X_i(n,i))==1)
	{
		cout<<"执行指针右移"<<endl; 
		goto R;
    }
    //如果右部为2，执行左移指令 
	if(r(X_i(n,i))==2)
    {
    	cout<<"执行指针左移"<<endl; 
    	goto L;
    }
    //若右部为3，执行写1指令 
	if(r(X_i(n,i))==3)
    {
    	cout<<"执行写1操作"<<endl; 
    	goto F;//1
    }
    //若右部为4，执行写B指令 
    if(r(X_i(n,i))==4)
    {
    	cout<<"执行写B操作"<<endl; 
    	goto B;//B
    }
    //若右部超过4，且模2余1，执行 TO Ai IF B 指令 
    if(r(X_i(n,i))>4&&(r(X_i(n,i))%2==1))
    {
    	cout<<"执行TO Ai IF B"<<endl; 
    	goto T;
    	
    }
    //若右部超过4，且模2余0，执行 TO Ai IF 1 指令 
    if(r(X_i(n,i))>4&&(r(X_i(n,i))%2==0))
	{
		cout<<"执行TO Ai IF 1"<<endl; 
		goto H;
	
	}
	goto D; 

	D: 
	i=i+1;
	cout<<"执行下一个指令"<<endl;
	goto A;
	

	C:
	w=(r(X_i(n,i))-3)/2;
	cout<<"w="<<w<<endl;
	for(int t=1;t<=Lt(n);t++)
	{
		if(l(X_i(n,t))==w)
		{
			j=r(X_i(n,t));//取出对应的右部，执行相应的指令 
			cout<<"j="<<j<<endl;
			break;
		}
	}
	//根据右部的值判断 
	if(j==1)
	{
		goto R;//右移 
	} 
	if(j==2)
	{
		goto L;//左移 
	} 
	if(j==3)
	{
		goto F;//写1 
	}
	if(j==4)
	{
		goto B;//写B 
	}
	if(j>4&&j%2==1)
	{
		goto T;//执行TO Ai IF B
	}
	if(j>4&&j%2==0)
	{
		goto H;//执行TO Ai IF 1
	}

//执行TO Ai IF B
	T:
	if(*p=='B')
	{
		cout<<"执行C"<<endl; 
		goto C;
	}
	goto D;
//执行TO Ai IF 1
	H:
	if(*p=='1')
	{
		goto C;
	}
	goto D;
//执行写B指令，如果已经为B不用再写 
	B:
	if(*p=='B')
	{
		cout<<"执行D"<<endl; 
		goto D;
	}
	else 
	{
		*p='B';
	}
	goto D;
//执行写1的指令，如果已为1则不用再写 
	F:
	if(*p=='1')
	{
		cout<<"执行D"<<endl; 
		goto D;
	}
	else 
	{
		*p='1';
	}
	goto D;
//判断当前指针是否是在左端，如果是，不需要再左移 
	L:
	if(p!=belt)
	{
		cout<<"执行M"<<endl; 
		goto M;
	}
	goto D;
//指针左移 
	M:
	p=p-1;
	goto D;
//判断当前指针是否是在右端，如果是，不需要再右移 
	R:
	if(p!=belt+99)
	{
		cout<<"执行S"<<endl; 
		goto S;
	}
	goto D;
//指针右移 
	S:
	p=p+1;
	goto D;
//计算带上1的个数 
	G:
	int y=0;
	for(int a=0;a<100;a++)
	{
		cout<<belt[a]<<" ";
		if(belt[a]=='1')
		{
			y++;
		}
	}	
	cout<<"res="<<y<<endl;

} 
