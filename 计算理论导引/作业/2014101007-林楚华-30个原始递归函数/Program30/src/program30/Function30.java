package program30;

public class Function30 {

	public static void main(String[] args) {
		int x=10;
		int y=10;
		int z=2;
		int text[] = null;
		int res;
		//long res2;
		boolean res_b;
		System.out.println("n=10  x=10  y=10  z=2  c=2");
		res=add(x,y);
		System.out.printf("01 add(x,y): %d\n",res);
		
		res=mul(x,y);
		System.out.printf("02 mul(x,y): %d\n",res);
		
		res=fac(x);
		System.out.printf("03 fac(x): %d\n",res);
		
		res=exp(x,y);
		System.out.printf("04 exp(x,y): %d\n",res);
		
		res=P(x);
		System.out.printf("05 P(x): %d\n",res);
		
		res=sub(x,y);
		System.out.printf("06 sub(x,y): %d\n",res);
		
		res=abs(x,y);
		System.out.printf("07 abs(x,y): %d\n",res);
		
		res=a(x);
		System.out.printf("08 a(x): %d\n",res);
		
		res=h_f(text,y);
		System.out.printf("09 h_f(x,y): %d\n",res);
		
		res=g_f(text,y);
		System.out.printf("10 g_f(x,y): %d\n",res);
		
		
		int res_c;
		
		res=d(x,y);
		System.out.printf("11 d(x,y): %d\n",res);
		
		res_b=x_y(x,y);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("12 x_y(x,y): %d\n",res_c);
		
		res_b=xy_a_sub(x,y);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("13 xy_a_sub(x,y): %d\n",res_c);
		
		res_b=xsy(x,y);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("14 xsy(x,y): %d\n",res_c);
		
		res_b=yy_divided(x,y);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("15 yy_divided(x,y): %d\n",res_c);
		
		res=xy_divided(x,y);
		System.out.printf("16 xy_divided(x,y): %d\n",res);
		
		res_b=prim(x);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("17 prim(x): %d\n",res_c);
		
		res=P_i(x);
		System.out.printf("18 P_i(x): %d\n",res);
		res=R(x,y);
		System.out.printf("19 R(x,y): %d\n",res);
		res=t(x);
		System.out.printf("20 t(x): %d\n",res);
		
		
		res=x_i(x,z);
		System.out.printf("21 x_i(x,y): %d\n",res);
		res=Lt(x);
		System.out.printf("22 Lt(x): %d\n",res);
		
		res_b=GN(x);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("23 GN(x): %d\n",res_c);
		
		
		int[] a; 
		a=Godel(10);
		System.out.printf("24 Godel(x):");
		for(int i=0;i<a.length;i++){
			System.out.printf("%d ",a[i]);
		}
		System.out.printf("\n");
		
		
		int[] b;
		b=Godel_mul(a,a);
		System.out.printf("25 Godel_mul(x,y):");
		for(int i=0;i<b.length;i++){
			System.out.printf("%d ",b[i]);
		}
		System.out.printf("\n");
		
		res=x_a_num(x,1);
		System.out.printf("26 x_a_num(x,c): %d\n",res);
		
		res=cantor(x,y);
		System.out.printf("27 cantor(x,y): %d\n",res);
		
		res=r(x);
		System.out.printf("28 r(x): %d\n",res);
		
		res=L(x);
		System.out.printf("29 L(x): %d\n",res);
		
		res_b=prog(x);
		if(res_b) res_c=1;
		else res_c=0;
		System.out.printf("30 prog(x): %d\n",res_c);
	}

	private static int S(int x){
		return x+1;
	}
	
	//01
	private static int add(int x,int y){
		int sum = x+y;
		return sum;
	}
	//02
	private static int mul(int x,int y){
		int mul = x*y;
		return mul;
	}
	//03
	private static int fac(int x){
		if(x==0){
			int fac = S(x);
			return fac;
		}
		return mul(fac(x-1),S(x-1));
	}
	//04
	private static int exp(int x,int y){
		int exp=x;
		if(y==0){
			exp = 1;
		}
		else {
			for(int i=1;i<y;i++)
				exp*=x;
		}
		return exp;
	}
	//05
	private static int P(int x){
		if(x>0) return x-1;
		else return 0;
	}
	
	
	
	//06
	private static int sub(int x,int y){
		if(x>=y) return x-y;
		else return 0;
	}
	//07
	private static int abs(int x,int y){
		return sub(x,y)+sub(y,x);
	}
	//08
	private static int a(int x){
		if(x==0) return 1;
		else return 0;
	}
	//09
	private static int f(int[] x,int t){
		return 1;
	}
	private static int h_f(int[] x,int y){
		if(y==0) return f(x,0);
		 return f(x,y)+h_f(x,y-1);
	}
	//10
	private static int g_f(int[] x,int y){
		if(y==0) return f(x,0);
		 return f(x,y)*g_f(x,y-1);
	}
	
	
	//11 x=y的特征函数d(x,y)
	private static int d(int x,int y){
		if(x==y) return 0;
		else return 1;
	}
	//12 x=y
	private static boolean x_y(int x,int y){
		if(d(x,y)==0) return true;
		else return false;
		}
	//13 x>y
	private static boolean xy_a_sub(int x,int y){
		if(1-a(sub(x,y))==1) return true;
		else return false;
	}
	//14 x<=y
	private static boolean xsy(int x,int y){
		if((x<y)||(x==y)) return true;
		else return false;
	}
	//15整除y*i==x(x>y)
	private static boolean yy_divided(int x,int y){
		int temp=0;
		for(int i=1;i<=x;i++){
			if(y*i==x) {
				temp=i;
				break;
			}
		}
		if(temp==0) return false;
		else return true;
		//return temp
	}
	//16取整
	private static int xy_divided(int x,int y){
		return y/x;
	}
	
	//17 x是素数
	private static boolean prim(int x){
		int j=0;
		if(x==1) return false;
		if(x==2 || x==3) return true;
		else{
		//若分解出其他因子，j为出1和x以外的其他因子个数
		for(int i=2; i<x; i++){
			boolean temp= yy_divided(x,i);
			if(temp) j+=1;
		}
		
		return j==0;
		}
	}

	//18 P(i)第i个素数
	private static int P_i(int i){
		int j=i;
		int temp=1;
		while(j>=1){
			if(prim(temp)){
				j--;
				//System.out.printf("%d ",temp);
			}
			temp++;
		}
		return temp-1;
	}
	
	//19 R(x,y)取余
	private static int R(int x,int y){
		return x%y;
	}
	
	//20 t(x)在x的素因子分解中非零指数的个数=Lh(x,x)
	//Lh(i,x)
	private static int Lh(int i,int x){
		int res;
		if(i==0) return 0;
		else if(yy_divided(x,P_i(i))){
			res =Lh(i-1,x)+1;
			return res;
		}
		else {
			res =Lh(i-1,x);
			return res;
			}
	}
	//t(x)
	private static int t(int x){
		return Lh(x,x);
	}
	
	
	//21 (x)i  x_i(x,i):x的素因子分解中第i个素数的指数
	//x_t(x,t)x的t次方
	private static int x_t(int x,int t){
		int res=1;
		int j;
		if(t==0) return 1;
		else{
		for(j=t;j>=1;j--){
			res*=x;
		}
		return res;
		}
	}
	//x_i(x,i)
	private static int x_i(int x,int i){
		int pi=P_i(i);//第i个素数
		//System.out.printf("%d ",pi);
		int t=0;
		while(yy_divided(x,x_t(pi,t))){
			t++;
		}
		return t-1;
		
		
	}
	
	//22 Lt(x) x的素因子分解中，非0指数的最大素数的序号
	private static int Lt(int x){
		int t=1;
		int a=x_i(x,t);          //x分解中第一个素数的指数
		//System.out.printf("%d ",a);
		int pi_a=x_t(P_i(t),a);  //第一个素数的指数次方
		//System.out.printf("%d ",pi_a);
		int shang=x/pi_a;//取整
		//System.out.printf("%d ",shang);
		while(shang!=1 && t<x){
			t++;
			shang=shang/x_t(P_i(t),x_i(x,t));
			//System.out.printf("%d ",shang);
		}
		return t;
	}
	
	//23 GN(x) Lt(x)内没有指数为0的素因子
	private static boolean GN(int x){
		int count=0;
		for(int i=1;i<=Lt(x);i++){
			if(x_i(x,i)==0) count++;
		}
		if(count==0) return true;
		else return false;
	}
	
	//24 Godel(x)得到哥德尔数数组
	private static int[] Godel(int x){
		int length;
		length=Lt(x);
		int[] a= new int[length];
		for(int i=1;i<=length;i++){
			a[i-1]=x_i(x,i);
		}
		return a;
	}
	
	//25 Godel_mul(x,y)
	private static int[] Godel_mul(int[] x,int[] y){
		int length;
		int j=0;
		length=x.length+y.length;
		int[] a= new int[length];
		for(int i=0;i<x.length;i++){
			a[i]=x[i];
		}
		for(int i=x.length;i<length;i++){
			a[i]=y[j++];
		}
		return a;
	}
	
	//26 x_a_num x的素因子分解中指数为a的数因子个数
	private static int x_a_num(int x,int a){

		int res=0;
		for(int i=1;i<=Lt(x);i++){
			if(x_i(x,i)==a) res++;
		}
		return res;
	}
	
	
	//27 cantor_1 上对角矩阵中元素a_xy的cantor对角线编码
	private static int cantor(int x,int y){
		return (x+y)*(x+y+1)/2+y;
	}
	
	//28 r(Z) 从一个数唯一求出两个数，取右部
	//h(n)=1+2+...+n
	private static int h(int n){
		if(n<=1) return 1;
		return n+h(n-1);
	}
	private static int r(int z){
		int n;
		for(n=1;n<=z;n++){
			if((h(n)<=z)&&(z<h(n+1)))
				break;
		}
		return z-h(n);
	}
	
	//29 L(z) 从一个数唯一求出两个数，取左部
	private static int L(int z){
		int n;
		for(n=0;n<=z;n++){
			if((h(n)<=z)&&(z<h(n+1)))
				break;
		}
		return n-r(z);
	}
	
	//30 prog(x)若x是某一P-T程序的Godel数，则谓词为真，否则为假
	private static boolean prog(int x){

		int length=Lt(x);
		int[] a= new int[length];
		a=Godel(x);
	
		//boolean temp=true;
		int count=0;
		
		for(int i=1;i<=length;i++){
			if(r(a[i-1])==0){
				count++;
				return false;
			}
		}
		
		for(int i=1;i<=length;i++){
			if(a[i-1]==0){
				count++;
				return false;
			}
		}
		
		for(int i=1;i<=length;i++)
			for(int j=1;j<=length;j++){
			if(L(a[i])!=0 && L(a[j])!=0)
			if(L(a[i])==L(a[j])){
				count++;
				return false;
			}
		}
		
		if(count==0)
			return true;
		else 
			return false;
		
		
		

	}
	
	
}











