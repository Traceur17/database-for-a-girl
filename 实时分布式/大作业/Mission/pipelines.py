# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import xlwt

class MissionPipeline(object):

    wbk = xlwt.Workbook()
    sheet = wbk.add_sheet('xinsanban')
    i = 0

    def __init__(self):
        self.sheet.write(0,0,'代码')
        self.sheet.write(0,1,'名称')
        self.sheet.write(0,2,'最新价')
        self.sheet.write(0,3,'涨跌')
        self.sheet.write(0,4,'涨跌幅(%)')
        self.sheet.write(0,5,'成交量(万股)')
        self.sheet.write(0,6,'成交量(万元)')
        self.sheet.write(0,7,'昨收')
        self.sheet.write(0,8,'今开')
        self.sheet.write(0,9,'最高价')
        self.sheet.write(0,10,'最低价')
        self.sheet.write(0,11,'市盈率')
        self.sheet.write(0,12,'总市值(万元)')
        self.i = 0

    def process_item(self, item, spider):
        self.i += 1
        self.sheet.write(self.i,0,item['Code'])
        self.sheet.write(self.i,1,item['Name'])
        self.sheet.write(self.i,2,item['Close'])
        self.sheet.write(self.i,3,item['Change'])
        self.sheet.write(self.i,4,item['ChangePercent'])
        #need some change
        self.sheet.write(self.i,5,item['Volume'])
        self.sheet.write(self.i,6,item['Amount'])
        self.sheet.write(self.i,7,item['PreviousClose'])
        #need some change
        self.sheet.write(self.i,8,item['Open'])
        self.sheet.write(self.i,9,item['High'])
        self.sheet.write(self.i,10,item['Low'])
        self.sheet.write(self.i,11,item['PERation'])
        self.sheet.write(self.i,12,item['MarketValue'])
        return item

    def close_spider(self,spider):
        self.wbk.save('xinsanbantest.xls')
        pass
