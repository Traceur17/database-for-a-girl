import scrapy
from bs4 import BeautifulSoup
from Mission.items import MissionItem
import re


class start_spider(scrapy.Spider):

    name = "start"
    start_urls = ["http://xinsanban.eastmoney.com/api/QuoteCenter/GPGS/GetGPGS?level=1&page=1&pagesize=20&sortType=ChangePercent&sortRule=-1"]

    def parse(self,response):

        url1 = 'http://xinsanban.eastmoney.com/api/QuoteCenter/GPGS/GetGPGS?level=1&page='
        url2 = '&pagesize=20&sortType=ChangePercent&sortRule=-1'

        soup = BeautifulSoup(response.text, "html.parser")
        allpage = soup.find('totalpage').string

        for i in range(1,int(allpage)+1):
            try:
                url = url1 + str(i) + url2
                yield scrapy.Request(url,callback=self.parseurl)
            except:
                continue

    def parseurl(self,response):

        soup = BeautifulSoup(response.text,"html.parser")
        for each in soup.find('result').children:
            item = MissionItem()

            item['Code'] = each.find('d2p1:code').string
            item['Name'] = each.find('d2p1:name').string
            item['Close'] = each.find('d2p1:close').string
            item['Change'] = each.find('d2p1:change').string
            item['ChangePercent'] = each.find('d2p1:changepercent').string
            # need some change
            item['Volume'] = each.find('d2p1:volume').string
            item['Amount'] = each.find('d2p1:amount').string
            item['PreviousClose'] = each.find('d2p1:previousclose').string
            # need some change
            item['Open'] = each.find('d2p1:open').string
            item['High'] = each.find('d2p1:high').string
            item['Low'] = each.find('d2p1:low').string
            item['PERation'] = each.find('d2p1:peration').string
            item['MarketValue'] = each.find('d2p1:marketvalue').string

            #print(item['Code'],item['Name'],item['Close'],item['Change'],item['ChangePercent'],item['Volume'],item['Amount'],item['PreviousClose'],item['Open'],item['High'],item['Low'],item['PERation'],item['MarketValue'])

            yield item