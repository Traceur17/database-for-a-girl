# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MissionItem(scrapy.Item):

    Code = scrapy.Field()
    Name = scrapy.Field()
    Close = scrapy.Field()
    Change = scrapy.Field()
    ChangePercent = scrapy.Field()
    Volume = scrapy.Field()
    Amount = scrapy.Field()
    PreviousClose = scrapy.Field()
    Open = scrapy.Field()
    High = scrapy.Field()
    Low = scrapy.Field()
    PERation = scrapy.Field()
    MarketValue = scrapy.Field()
